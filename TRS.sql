-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: train_reservation
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `price` double DEFAULT NULL,
  `seats` int NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `train_no` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKkr3g1o31kfvc9mruyndt8r1y2` (`type`,`train_no`),
  KEY `FKga7a3mseoomyp9gqdkb140efh` (`train_no`),
  CONSTRAINT `FKga7a3mseoomyp9gqdkb140efh` FOREIGN KEY (`train_no`) REFERENCES `train` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class`
--

LOCK TABLES `class` WRITE;
/*!40000 ALTER TABLE `class` DISABLE KEYS */;
INSERT INTO `class` VALUES (1,200,244,'GENERAL',1),(2,300,296,'SLEEPER',1),(3,600,100,'GENERAL',2),(4,800,200,'SLEEPER',2),(5,950,210,'AC_TIER1',2),(6,120,99,'GENERAL',3),(7,200,250,'SLEEPER',3),(8,300,200,'GENERAL',4),(9,550,200,'SLEEPER',4),(10,850,250,'AC_TIER1',4),(11,150,200,'GENERAL',5),(12,250,200,'SLEEPER',5),(14,300,249,'SLEEPER',6),(15,250,200,'SLEEPER',7),(16,150,230,'GENERAL',7);
/*!40000 ALTER TABLE `class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal`
--

DROP TABLE IF EXISTS `meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meal` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `price` double NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `pnr_no` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKh5pi4mrbqrvfk7qre073jgm59` (`pnr_no`),
  CONSTRAINT `FKh5pi4mrbqrvfk7qre073jgm59` FOREIGN KEY (`pnr_no`) REFERENCES `ticket` (`pnr_no`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal`
--

LOCK TABLES `meal` WRITE;
/*!40000 ALTER TABLE `meal` DISABLE KEYS */;
INSERT INTO `meal` VALUES (1,300,'VEG_THALI',1111111111),(2,300,'VEG_THALI',1111111112),(3,500,'NONVEG_THALI',1111111112),(4,500,'NONVEG_THALI',1111111113),(5,300,'VEG_THALI',1111111114),(6,300,'VEG_THALI',1111111116),(7,500,'NONVEG_THALI',1111111118),(8,300,'VEG_THALI',1111111119);
/*!40000 ALTER TABLE `meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passenger`
--

DROP TABLE IF EXISTS `passenger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passenger` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `age` int NOT NULL,
  `psgr_name` varchar(255) DEFAULT NULL,
  `seat_pref` varchar(255) DEFAULT NULL,
  `pnr_no` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi4mn6kbkqt4xjdpfoq22nl5un` (`pnr_no`),
  CONSTRAINT `FKi4mn6kbkqt4xjdpfoq22nl5un` FOREIGN KEY (`pnr_no`) REFERENCES `ticket` (`pnr_no`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passenger`
--

LOCK TABLES `passenger` WRITE;
/*!40000 ALTER TABLE `passenger` DISABLE KEYS */;
INSERT INTO `passenger` VALUES (1,27,'Pankaj Bodade','upper',1111111111),(2,25,'Akash Helale','lower',1111111111),(3,25,'Abhijeet Pal','upper',1111111112),(4,26,'Aniket Madwe','upper',1111111112),(5,28,'Bhushan Tadele','upper',1111111112),(6,24,'Shubham Khalkar','upper',1111111113),(7,32,'Suresh Mane','lower',1111111114),(8,22,'Akshit Kumar','lower',1111111115),(9,50,'Rajiv Sir','upper',1111111116),(11,55,'Mahesh Bro','upper',1111111118);
/*!40000 ALTER TABLE `passenger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment` (
  `trans_id` bigint NOT NULL,
  `amount` double NOT NULL,
  `mode` varchar(50) DEFAULT NULL,
  `pnr_no` bigint NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `FK4vj574406i6ykbldj5devyq15` (`pnr_no`),
  CONSTRAINT `FK4vj574406i6ykbldj5devyq15` FOREIGN KEY (`pnr_no`) REFERENCES `ticket` (`pnr_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` VALUES (10000000,700,'upi',1111111111),(10000001,1700,'debit card',1111111112),(10000002,620,'upi',1111111113),(10000003,600,'upi',1111111114),(10000004,200,'cash',1111111115),(10000005,500,'UPI',1111111116),(10000006,300,'Card',1111111117),(10000007,700,'Net Banking',1111111118),(10000008,500,'UPI',1111111119);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `route` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `arr_time` time DEFAULT NULL,
  `current_station` varchar(255) DEFAULT NULL,
  `dept_time` time DEFAULT NULL,
  `next_station` varchar(255) DEFAULT NULL,
  `station_no` int DEFAULT NULL,
  `schedule_id` bigint DEFAULT NULL,
  `train_no` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk8sjhra3fe786rdfi9buvl19w` (`schedule_id`),
  KEY `FK83x9cejcrn67pxyudihwo59cq` (`train_no`),
  CONSTRAINT `FK83x9cejcrn67pxyudihwo59cq` FOREIGN KEY (`train_no`) REFERENCES `train` (`id`),
  CONSTRAINT `FKk8sjhra3fe786rdfi9buvl19w` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (1,'01:30:00','Kolhapur','01:30:00','Pune',1,1,1),(2,'04:05:00','Pune','04:15:00','Nagpur',2,1,1),(3,'12:00:00','Nagpur','13:15:00','Gondia',3,1,1),(4,'16:50:00','Gondia','16:50:00','Gondia',4,1,1),(5,'05:50:00','Mumbai','05:50:00','Nashik',1,2,2),(6,'08:50:00','Nashik','09:10:00','Bhopal',2,2,2),(7,'02:48:00','Bhopal','15:08:00','Delhi',2,2,2),(8,'10:50:00','Delhi','10:50:00','Delhi',4,2,2),(9,'10:35:00','Pune','10:35:00','Lonavala',1,3,3),(10,'11:25:00','Lonavala','11:35:00','Mumbai',2,3,3),(11,'12:55:00','Mumbai','12:56:00','Mumbai',3,3,3),(12,'01:00:00','Chennai','01:00:00','Gulbarga',1,4,4),(13,'05:30:00','Gulbarga','05:40:00','Pune',2,4,4),(14,'09:40:00','Pune','09:55:00','Mumbai',3,4,4),(15,'12:50:00','Mumbai','12:50:00','Mumbai',4,4,4),(16,'13:20:00','Latur','01:20:00','Mumbai',1,5,5),(17,'17:18:00','Mumbai','17:18:00','Mumbai',2,5,5),(18,'03:09:00','Kolhapur','03:13:00','Pune',1,6,6),(19,'04:10:00','Pune','04:20:00','Mumbai',2,6,6),(20,'08:10:00','Mumbai','08:10:00','Mumbai',3,6,6);
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `destination` varchar(50) DEFAULT NULL,
  `source` varchar(50) DEFAULT NULL,
  `train_no` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKe7mijixgm96it6wmkqcoufopx` (`date`,`train_no`),
  KEY `FKsy47kucxuallcqps8eod2kmbt` (`train_no`),
  CONSTRAINT `FKsy47kucxuallcqps8eod2kmbt` FOREIGN KEY (`train_no`) REFERENCES `train` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (1,'2022-09-20','Gondia','Kolhapur',1),(2,'2022-09-25','Delhi','Mumbai',2),(3,'2022-09-25','Mumbai','Pune',3),(4,'2022-09-25','Mumbai','Chennai',4),(5,'2022-09-20','Mumbai','Latur',5),(6,'2022-09-19','Mumbai','Kolhapur',6),(7,'2022-09-25','Mumbai','Pune',7);
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `pnr_no` bigint NOT NULL,
  `dest` varchar(50) DEFAULT NULL,
  `fare` double NOT NULL,
  `src` varchar(50) DEFAULT NULL,
  `t_date` date DEFAULT NULL,
  `t_time` time DEFAULT NULL,
  `class_id` bigint DEFAULT NULL,
  `train_no` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  PRIMARY KEY (`pnr_no`),
  KEY `FKhn5xwbqpggofvi0wv6dovdvfd` (`class_id`),
  KEY `FKqyy2b8reou5fdhr906yoajd4t` (`train_no`),
  KEY `FKdvt57mcco3ogsosi97odw563o` (`user_id`),
  CONSTRAINT `FKdvt57mcco3ogsosi97odw563o` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKhn5xwbqpggofvi0wv6dovdvfd` FOREIGN KEY (`class_id`) REFERENCES `class` (`id`),
  CONSTRAINT `FKqyy2b8reou5fdhr906yoajd4t` FOREIGN KEY (`train_no`) REFERENCES `train` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1111111111,'Gondia',700,'Kolhapur','2022-09-20','01:30:00',1,1,4),(1111111112,'Pune',1700,'Kolhapur','2022-09-20','01:30:00',2,1,5),(1111111113,'Mumbai',620,'Pune','2022-09-25','10:35:00',6,3,5),(1111111114,'Mumbai',600,'Kolhapur','2022-09-20','02:08:00',14,6,7),(1111111115,'Pune',200,'Kolhapur','2022-09-20','01:30:00',1,1,2),(1111111116,'Gondia',500,'Kolhapur','2022-09-20','01:30:00',1,1,7),(1111111117,'Nagpur',300,'Kolhapur','2022-09-20','01:30:00',2,1,7),(1111111118,'Pune',700,'Kolhapur','2022-09-20','01:30:00',1,1,7),(1111111119,'Gondia',500,'Kolhapur','2022-09-20','01:30:00',1,1,7);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `train`
--

DROP TABLE IF EXISTS `train`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `train` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `train_name` varchar(100) DEFAULT NULL,
  `train_no` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5kdorev80dwqi2uem9f401u0e` (`train_name`),
  UNIQUE KEY `UK_ef1wt60orelou92j38obduf38` (`train_no`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `train`
--

LOCK TABLES `train` WRITE;
/*!40000 ALTER TABLE `train` DISABLE KEYS */;
INSERT INTO `train` VALUES (1,'Maharashtra Express',11040),(2,'Rajdhani Express',11060),(3,'Intercity Express',12128),(4,'Chennai Express',12163),(5,'Latur Express',11010),(6,'Deccan Queen Express',11050),(7,'MH Express',11052);
/*!40000 ALTER TABLE `train` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `city` varchar(15) DEFAULT NULL,
  `dob` date NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `f_name` varchar(30) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `l_name` varchar(30) DEFAULT NULL,
  `mobile_no` bigint DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `pin` int DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `sec_ans` varchar(100) DEFAULT NULL,
  `sec_ques` varchar(100) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKr8vfnr9vwcnjdjn1cxr9k76rc` (`f_name`,`l_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Noida','2000-01-01','admin@gmail.com','Admin','male','TRS',8668735703,'$2a$10$XU/nZlt3e3lfEhjrjfACbe1nT/d.cJVOOEgFgByoUNqT6GDoMyyS.',542155,'ADMIN','Admin','what is your nickname?','Uttar Pradesh'),(2,'Mumbai','1995-01-05','clerk@gmail.com','Clerk','male','TRS',9960830560,'$2a$10$.krlZChN0WRGuvs7UbcnyufPd4lMNTwFihj42ciYFfCMcm/pq7Mka',4100002,'CLERK','Clerk','what is your nickname?','Maharashtra'),(4,'Latur','1999-01-01','omkar.bagade37@gmail.com','Amit','male','Jirole',8762184514,'$2a$10$feBr93XFeskapq.FgR2PROHxutr90oTW6eSAJkLm3E7c4b8evrhGe',451120,'USER','Amit','what is your nickname?','Maharashtra'),(5,'Latur','1997-01-03','akashhelale3@gmail.com','Akash','male','Helale',8862184514,'$2a$10$S9nlGFCZmfx3TzUGSRZnZ.wUXd40bEtNFFXiK5JJDbnamLzedMKEi',451120,'USER','aniket','what is your nickname?','Maharashtra'),(6,'Buldhana','1995-10-09','pankaj.bodade129@gmail.com','Pankaj','male','Bodade',8962184514,'$2a$10$p8Rfo1MQ1dGY6qaCYA0vj.NzDMmVnYfTJr3CMmzFHBojWMFWIEqZ2',406450,'USER','Pankaj','what is your nickname?','Maharashtra'),(7,'Pune','1995-06-06','omkarabagade@gmail.com','Omkarb','male','Bagade',9762184514,'$2a$10$lWwZ5.Weosq.1NEyULT4ZOM1p.6HDQ7Ro7qUwEo6F5Gx6ErfScC8C',412102,'USER','om','what is your nickname?','Maharashtra');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-24 17:06:10
